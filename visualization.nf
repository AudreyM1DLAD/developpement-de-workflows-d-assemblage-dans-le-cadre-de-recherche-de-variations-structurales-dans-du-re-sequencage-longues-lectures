#!~/achathua/bin/nextflow

nextflow.enable.dsl = 2

process circos {
	publishDir "${circos_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path bed_file 
        	val circos_outdir
        
    	output:
        	path "*"
        		
	script:
	"""
	#!/usr/bin/bash
	
	module unload python
	pip install pycirclize 
	module load python/3.11
		
	python ${params.scripts}/pycirclize_ragoo.py ${bed_file}
	
	"""

}


// NOT FUNCTIONAL
process samplot {
	publishDir "${samplot_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path assembly_fasta
        	val assembly_outdir 
        	val samplot_outdir
        
    	output:
        	path "*"
        		
	script:
	"""
	#!/usr/bin/python 
	
	"""

}


// NOT FUNCTIONAL
process momig { 
	publishDir "${momig_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path assembly_fasta
        	val assembly_outdir 
        	val ragoo_outdir
        
    	output:
        	path "ragoo_output/pm_alignments/assemblytics_out.Assemblytics_structural_variants.bed"
        		
	script:
	"""
	#!/usr/bin/python 
	
	"""
}