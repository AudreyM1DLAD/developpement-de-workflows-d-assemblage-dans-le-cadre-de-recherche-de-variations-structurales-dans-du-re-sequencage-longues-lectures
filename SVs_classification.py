 #!/usr/bin/env python3

import sys
import pandas as pd
import numpy as np

# Import the bed
variants_file = sys.argv[1]  
variants_data = pd.read_csv(variants_file, sep='\t', header=0)

# Remove useless columns
SVs_features = variants_data[["reference", "size", "type"]]
SVs_features["class"]=""


# Numpy method to attribute a class to the SVs depending on their size

conditions = [
    (SVs_features["size"] >= 50) & (SVs_features["size"] <= 250),
    (SVs_features["size"] > 250) & (SVs_features["size"] <= 500),
    (SVs_features["size"] > 500) & (SVs_features["size"] <= 1000),
    (SVs_features["size"] > 1000) & (SVs_features["size"] <= 20000),
    SVs_features["size"] > 20000,
]

categories = ["S","M","L","XL","XXL"]

SVs_features["class"] = np.select(conditions, categories, default='Unclassified')

#New df for the number of SVs in each classes
nb_S = SVs_features["class"].value_counts()["S"]
nb_M = SVs_features["class"].value_counts()["M"]
nb_L = SVs_features["class"].value_counts()["L"]
nb_XL = SVs_features["class"].value_counts()["XL"]
nb_XXL = SVs_features["class"].value_counts()["XXL"]
Total = nb_S + nb_M + nb_L + nb_XL + nb_XXL

SVs_classes_df = pd.DataFrame(
    data=[nb_S, nb_M, nb_L, nb_XL, nb_XXL, Total], 
    index=["S","M","L","XL","XXL", "Total"], 
    columns=['Number of SVs']
    )
SVs_classes_df.index.name = "Class"

SVs_classes_df.to_csv('SVs_classes.csv', index=True)

#To read easily the CSV output in the Terminal : column -s, -t < SVs_classes.csv | less -#2 -N -S