#!~/achathua/bin/nextflow
nextflow.enable.dsl=2

// My contribution to the file sv_calling_assembly.nf 


// Processes Calling 
include { convert_gfa_to_fasta as hconvert1 } from './convert'
include { convert_gfa_to_fasta as hconvert2 } from './convert'
include { ragoo as flyragoo } from './ragoo'
include { ragoo as phragoo } from './ragoo'
include { ragoo as hragoo1 } from './ragoo'
include { ragoo as hragoo2 } from './ragoo'
include { ragoo_vcf as flyragoovcf } from './ragoo'
include { ragoo_vcf as phragoovcf } from './ragoo'
include { ragoo_vcf as hragoovcf1 } from './ragoo'
include { ragoo_vcf as hragoovcf2 } from './ragoo'
include { sniffles as flysniffles } from './othercallers'
include { sniffles as hsniffles1 } from './othercallers'
include { sniffles as hsniffles2 } from './othercallers'
include { quast_evaluation as flyquast } from './analysis'
include { quast_evaluation as phquast } from './analysis'
include { quast_evaluation as hquast1 } from './analysis'
include { quast_evaluation as hquast2 } from './analysis'
include { circos as flycircos } from './visualization'
include { circos as phcircos } from './visualization'
include { circos as hcircos1 } from './visualization'
include { circos as hcircos2 } from './visualization'
include { samplot as flysamplot } from './visualization'
include { samplot as hsamplot1 } from './visualization'
include { samplot as hsamplot2 } from './visualization'
include { momig as flymomig } from './visualization'
include { momig as hmomig1 } from './visualization'
include { momig as hmomig2 } from './visualization'



// This workflow was not written by me but I modifiy it by adding the processes call I develop

workflow WF_FLY_ASSEMBLY {
    take:
        fastq_ch
    main:
        extract_fasta_reads(fastq_ch)
        fly_assembly(extract_fasta_reads.out)
        flyragoo(fly_assembly.out.assemblyfa, "${params.assf}", "${params.rf}")
	    flyragoovcf(flyragoo.out, "${params.rf}")
	    flycircos(flyragoo.out, "${params.cf}")
	    flyquast(fly_assembly.out.assemblyfa, "${params.assf}", "${params.qf}")
}



// Originally on the implement-hifiasm branch that (we have to) merge to the main branch

workflow WF_HIFIASM_ASSEMBLY {
    take:
        fastq_ch
    main:
        hifiasm_assembly(fastq_ch)
	    hconvert1(hifiasm_assembly.out.hap1)
	    hconvert2(hifiasm_assembly.out.hap2)
	    //hragoo1(hconvert1.out, "${params.assh}", "${params.rhh1}")
	    //hragoo2(hconvert2.out, "${params.assh}", "${params.rhh2}")
        //hragoovcf1(hragoo1.out, "${params.rhh1}")
        //hragoovcf2(hragoo2.out, "${params.rhh2}")
        hsniffles1(hconvert1.out, "${params.assh}", "${params.shh1}", 1)
        hsniffles2(hconvert2.out, "${params.assh}", "${params.shh2}", 2)
        hcircos1(hsniffles1.out, "${params.chh1}")
        hcircos2(hsniffles2.out, "${params.chh2}")
        //hcircos1(hragoo1.out, "${params.chh1}")
        //hcircos2(hragoo2.out, "${params.chh2}")
        //hvg1(hifiasm_assembly.out.hap1, "${params.assh}", "${params.vhh1}")
        //hvg2(hifiasm_assembly.out.hap2, "${params.assh}", "${params.vhh2}")
	    hquast1(hconvert1.out, "${params.assh}", "${params.qhh1}")
	    hquast2(hconvert2.out, "${params.assh}", "${params.qhh2}")

}



// Originally on the implement-phasebook branch that (we have to) merge to the main branch

workflow WF_PHASEBOOK_ASSEMBLY {
    take:
        fastq_ch
    main:
        extract_fasta_reads(fastq_ch)
        phasebook_assembly(extract_fasta_reads.out)
        phragoo(phasebook_assembly.out.assemblyfa, "${params.assph}", "${params.rph}")
	    phragoovcf(phragoo.out, "${params.rph}")
	    phcircos(phragoo.out, "${params.cph}")
	    phquast(phasebook_assembly.out.assemblyfa, "${params.assph}", "${params.qph}")
}



// This process was not written by me but I modifiy it by adding te strategy for ressources and naming the output

process fly_assembly {
	
	executor 'slurm'
    	memory { 1.GB * task.attempt }
    	time { 4.hour * task.attempt }
    	errorStrategy 'retry'
    	
	publishDir "${params.sampledir}/ASSEMBLY/FLY", mode: 'copy', overwrite: true
	
	input:
        	path fasta_reads
        
    	output:
		path "assembly.fasta", emit: assemblyfa
    	
	script:
	"""
    	#!/usr/bin/bash 
    	
	if [ ! -d ${params.sampledir}/ASSEMBLY/FLY ]
	then mkdir '${params.sampledir}/ASSEMBLY/FLY'
	fi
	
	module unload python
	module load flye
	
	if [ ${params.tech} == "ONT" ]
	then flye --nano-raw ${fasta_reads} -g ${params.glen} -o . -t ${task.cpus}
	elif [ ${params.tech} == "PACBIO" ]
	then flye --pacbio-hifi ${fasta_reads} -g ${params.glen} -o . -t ${task.cpus}
	elif [ ${params.tech} == "NGS" ]
	then echo "Assembly workflow is not implemented for NGS technology"
	fi
	
	"""
}


// Originally on the implement-hifiasm branch that (we have to) merge to the main branch

process hifiasm_assembly {
	
	executor 'slurm'
    	memory { 1.GB * task.attempt }
    	time { 4.hour * task.attempt }
    	errorStrategy 'retry'
	
	publishDir "${params.sampledir}/ASSEMBLY/HIFIASM", mode: 'copy', overwrite: true, pattern: "${params.sample}.*"
        
	input:
        	val fastq_ch
        	
	output:
        	path "${params.sample}.asm.bp.hap1.p_ctg.gfa", emit: hap1  
        	path "${params.sample}.asm.bp.hap1.p_ctg.lowQ.bed"
        	path "${params.sample}.asm.bp.hap1.p_ctg.noseq.gfa" 	
        	path "${params.sample}.asm.bp.hap2.p_ctg.gfa", emit: hap2
        	path "${params.sample}.asm.bp.hap2.p_ctg.lowQ.bed"
        	path "${params.sample}.asm.bp.hap2.p_ctg.noseq.gfa"
        	path "${params.sample}.asm.bp.p_ctg.gfa"
        	path "${params.sample}.asm.bp.p_ctg.lowQ.bed"
        	path "${params.sample}.asm.bp.p_ctg.noseq.gfa"
        	path "${params.sample}.asm.bp.p_utg.gfa"
        	path "${params.sample}.asm.bp.p_utg.lowQ.bed"
        	path "${params.sample}.asm.bp.p_utg.noseq.gfa"
        	path "${params.sample}.asm.bp.r_utg.gfa"
        	path "${params.sample}.asm.bp.r_utg.lowQ.bed"
        	path "${params.sample}.asm.bp.r_utg.noseq.gfa"
        	path "${params.sample}.asm.ec.bin"
        	path "${params.sample}.asm.ovlp.reverse.bin"
        	path "${params.sample}.asm.ovlp.source.bin" 	
        	
        script:

        """
        #!/usr/bin/bash
        
        module load hifiasm/0.19.9

	if [ ! -d ${params.sampledir}/ASSEMBLY ]
        then mkdir -p "${params.sampledir}/ASSEMBLY/HIFIASM"
	fi 

	hifiasm -o ${params.sample}.asm -t ${task.cpus} ${fastq_ch}

	"""
}


// Originally on the implement-phasebook branch that (we have to) merge to the main branch

process phasebook_assembly {

	executor 'slurm'
    	memory { 1.GB * task.attempt }
    	time { 4.hour * task.attempt }
    	errorStrategy 'retry'

        publishDir "${params.sampledir}/ASSEMBLY/PHASEBOOK", mode: 'copy', overwrite: true

        input:
        	path fasta_reads

	output:
		path "contigs.fa", emit: assemblyfa

        script:
        """
	#!/usr/bin/bash
        if [ ! -d ${params.sampledir}/ASSEMBLY/PHASEBOOK ]
                then mkdir -p "${params.sampledir}/ASSEMBLY/PHASEBOOK"
        fi

        module load phasebook/1.0.0

        if [ "${params.tech}" == 'ONT' ]
	then phasebook -i ${fasta_reads} -t ${task.cpus} -p ont -g large -x
	elif [ "${params.tech}" == 'PACBIO' ]
	then phasebook -i ${fasta_reads} -t ${task.cpus} -p hifi -g large -x
	fi

        """
}
