# Generate HG002_0_5X_PB.fastq.gz (datatest n°1)
    # DO
jobify -b -c 48 -p normal --time=500 --mem-per-cpu=3G "seqtk sample -s100 /env/cng/bigtmp/cjubin/HG002/HG002.fastq.gz 112304 | gzip > HG002_0_5X_PB.fastq.gz"
    # Check
jobify -b -c 48 -p normal --time=500 "seqkit stats HG002_0_5X_PB.fastq.gz -j 48 -o HG002_0_5X_PB.stats.gz"


# Generate HG002_1X_PB.fastq.gz (datatest n°2)
    # DO
jobify -b -c 48 -p normal --time=500 --mem-per-cpu=3G "seqtk sample -s100 /env/cng/bigtmp/cjubin/HG002/HG002.fastq.gz 224607 | gzip > HG002_1X_PB.fastq.gz"
    # Check
jobify -b -c 48 -p normal --time=500 "seqkit stats HG002_1X_PB.fastq.gz -j 48 -o HG002_1X_PB.fastq.gz"



# Generate patient_chr20_16X_ONT.fastq.gz (datatest n°3)
samtools view -b /env/cng/bigtmp/cjubin/NA12878_bam/C002RT1_PAM33173-PAM59439.sort.dup.bam chr20 >chr20.bam # Extract chr20 alignment
samtools view chr20.bam | wc –l # Check if chr20.bam is not empty. 
sambamba subsample --type fasthash --max-cov 16 -o chr20_16X.bam chr20.bam # Coverage
samtools fastq -o chr20_16X.fastq chr20_16X.bam # Conversion in fastq
mv chr20_16X.fastq patient_chr20_16X_ONT.fastq.gz # Rename correctly
