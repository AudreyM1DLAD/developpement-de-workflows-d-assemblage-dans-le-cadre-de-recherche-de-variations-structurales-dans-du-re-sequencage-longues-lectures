#!~/achathua/bin/nextflow
nextflow.enable.dsl=2



// The process ragoo already existed but I make it more general to add the possibility to handle several technologies (ONT, PacBio, NGS) and the ressources paragraph fo precaution


process ragoo {

	memory { 1.GB * task.attempt }
 	time { 4.hour * task.attempt }
 	errorStrategy 'retry'
	
	publishDir "${ragoo_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path assembly_fasta
        	val assembly_outdir 
        	val ragoo_outdir
        
    	output:
        	path "ragoo_output/pm_alignments/assemblytics_out.Assemblytics_structural_variants.bed"
        		
	script:
	"""
	#!/usr/bin/bash
	
	echo "RAGOO: assemblage= ${assembly_fasta}"
	
	#___INITIALISATION___ 
		
		#____Chargement des modules____
	    	module unload python
	    	module load python/3.7.6
	    	module load minimap2
	    	module load ragoo
	    	    	
		#_____Création du/des assembly.fasta spécifiques et SV_calling_____ 
		
		if [ ! -d ${ragoo_outdir} ]
			then mkdir -p "${ragoo_outdir}"
		fi 
		
		#______Création lien symbolique pour le FASTA assemblage______
							
		if [ ! -e assembly ]
			then ln -s ${assembly_outdir}/${assembly_fasta} assembly
		fi 
					
					
		#______Création lien symbolique pour la référence______ 
					
		if [ ! -e reference ]		
			then ln -s ${params.ref} reference
		fi
		
		    		
	#___SV_CALLING___ 	    		
	
		ragoo.py -t ${task.cpus} -s -m minimap2 assembly reference
					
		echo "SV_CALLING RAGOO : OK"
	
	"""
}




process ragoo_vcf {
	
   	memory { 1.GB * task.attempt }
 	time { 4.hour * task.attempt }
 	errorStrategy 'retry'
 	
   	publishDir "${ragoo_outdir}", mode: 'copy', overwrite: false, pattern: "*vcf"
   	
   	input:
		path ragoo_bed
		val ragoo_outdir
		
	output:
		path "*.vcf"
         
    	script:
 	""" 
 	
    	module load survivor
    	
    	SURVIVOR convertAssemblytics ${ragoo_bed} 50 ${ragoo_bed}.vcf
    	
    	for file in *.bed.vcf; do
    		mv "\$file" "\${file%.bed.vcf}.vcf"
	done
    	
    	echo "ragoo_vcf: ok"
    	
    	"""
}