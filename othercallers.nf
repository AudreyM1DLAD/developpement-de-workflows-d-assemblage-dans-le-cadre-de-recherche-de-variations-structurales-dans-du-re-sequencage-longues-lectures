#!~/achathua/bin/nextflow
nextflow.enable.dsl=2

process sniffles {

	publishDir "${sniffles_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path assembly_fasta
        	val assembly_outdir 
        	val sniffles_outdir
        	val haplotype_no
        output:
        	path "*"
        		
	script:
	"""
	#___INITIALISATION___ 
		#____Chargement des modules____
		module load minimap2 sniffles samtools
		
		
		#_____Création du/des assembly.fasta spécifiques_____ 	
		if [ ! -d ${sniffles_outdir} ]
			then mkdir -p "${sniffles_outdir}"
		fi 


	#___SV_CALLING___ 
		minimap2 -a ${params.ref} ${assembly_outdir}/${assembly_fasta} > hap${haplotype_no}.sam 2> minimap2_hap1.log
		
		samtools view -Sb hap${haplotype_no}.sam > hap${haplotype_no}.bam
		samtools sort hap${haplotype_no}.bam -o hap${haplotype_no}.sorted.bam
		samtools index hap${haplotype_no}.sorted.bam
		
		sniffles --input hap${haplotype_no}.sorted.bam --vcf hap${haplotype_no}.sv.vcf
	"""
}	

// NOT FUNCTIONAL
process pepper {

	publishDir "${pepper_outdir}", mode: 'copy', overwrite: true
	
	input:
        	path assembly_fasta
        	val assembly_outdir 
        	val pepper_outdir
        
        output: 
        	path ""
        	
       	script:
       	chen params.tech = ONT
       	"""
       	"""
}


// NOT FUNCTIONAL
process vg {
	
	publishDir "${vg_outdir}", mode: 'copy', overwrite: true
	
	input: 
        	path assembly_fasta
        	val assembly_outdir 
        	val vg_outdir
		val haplotype_no
		
	script: 
	"""
	#!/bin/bash 
	
	#___INITIALISATION___	
		#____Chargement des modules____	
		module load vg
		
		#_____Création du/des assembly.fasta spécifiques_____ 	
		if [ ! -d ${vg_outdir} ]
			then mkdir -p "${vg_outdir}"
		fi		

	#___ALIGNMENT PRE VARIANT CALLING___

		#____Import gfa file by converting him in vg file____
		vg convert -g ${assembly_outdir}/${assembly_fasta} > hap${haplotype_no}.vg
		
		# Fusionner les graphes
		vg merge hap1.vg hap2.vg > merged.vg

		# Indexer le graphe fusionné
		vg index -x merged.xg -g merged.gcsa -k 16 merged.vg
		
		#____Modifier le graphe importé____ 
		vg mod -X 256 hap1.vg > mod_hap1.vg

		#____Indexer le graph modifié____
		vg index -x mod_hap1.xg -g mod_hap1.gcsa -k 16 mod_hap1.vg

		#____General Mapping____ 
		vg map -m long -t 12 
		vg map -m long -t 12 -T fastQ? -x mod_hap1.xg -g mod_hap1.gcsa > hap1_aln.gam


	# __SV_CALLING__

		#____Compute the read support from the gam____
		# -Q 5: ignore mapping and base qualitiy < 5
		vg pack -x x.xg -g aln.gam -Q 5  -o aln.pack

		#____Generate a VCF from the support____  
		vg call x.xg -k aln.pack > graph_calls.vcf
	# Convertir GFA en VG





# Mapper les lectures FASTQ sur le graphe fusionné
vg map -x merged.xg -g merged.gcsa -f HG002_0_5X.fastq.gz > HG002.gam

# Emballer les lectures mappées
vg pack -x merged.xg -g HG002.gam -o HG002.pack

# Appeler les SVs
vg call merged.xg -k HG002.pack > sv_calls.vcf

	
	"""
}
        	