#!~/achathua/bin/nextflow
nextflow.enable.dsl=2

// My contribution to gexplor.nf

// Paramètres pour les PublishDir 
	// Assembly 
params.assf = "${params.sampledir}/ASSEMBLY/FLY"
params.assph = "${params.sampledir}/ASSEMBLY/PHASEBOOK"
params.assh = "${params.sampledir}/ASSEMBLY/HIFIASM" 
	// SV Calling 
params.rf = "${params.sampledir}/SV_CALLING/RAGOO/FLY"
params.rph = "${params.sampledir}/SV_CALLING/RAGOO/PHASEBOOK"
params.rhh1 = "${params.sampledir}/SV_CALLING/RAGOO/HIFIASM/HAP1"
params.rhh2 = "${params.sampledir}/SV_CALLING/RAGOO/HIFIASM/HAP2"
params.sf = "${params.sampledir}/SV_CALLING/SNIFFLES/FLY"
params.shh1 = "${params.sampledir}/SV_CALLING/SNIFFLES/HIFIASM/HAP1"
params.shh2 = "${params.sampledir}/SV_CALLING/SNIFFLES/HIFIASM/HAP2"
params.vf = "${params.sampledir}/SV_CALLING/VG/FLY"
params.vhh1 = "${params.sampledir}/SV_CALLING/VG/HIFIASM/HAP1"
params.vhh2 = "${params.sampledir}/SV_CALLING/VG/HIFIASM/HAP2"
params.pf = "${params.sampledir}/SV_CALLING/PEPPER/FLY"
params.phh1 = "${params.sampledir}/SV_CALLING/PEPPER/HIFIASM/HAP1"
params.phh2 = "${params.sampledir}/SV_CALLING/PEPPER/HIFIASM/HAP2"
	// Analyse  
params.qf = "${params.sampledir}/ANALYSIS/QUAST/FLY"
params.qph = "${params.sampledir}/ANALYSIS/QUAST/PHASEBOOK"
params.qhh1 = "${params.sampledir}/ANALYSIS/QUAST/HIFIASM/HAP1" 
params.qhh2 = "${params.sampledir}/ANALYSIS/QUAST/HIFIASM/HAP2"
	// Visualisation 
params.cf = "${params.sampledir}/VISUALIZATION/CIRCOS/FLY"
params.cph = "${params.sampledir}/VISUALIZATION/CIRCOS/PHASEBOOOK"
params.chh1 = "${params.sampledir}/VISUALIZATION/CIRCOS/HIFIASM/HAP1" 
params.chh2 = "${params.sampledir}/VISUALIZATION/CIRCOS/HIFIASM/HAP2"
params.spf = "${params.sampledir}/VISUALIZATION/SAMPLOT/FLY"
params.sphh1 = "${params.sampledir}/VISUALIZATION/SAMPLOT/HIFIASM/HAP1" 
params.sphh2 = "${params.sampledir}/VISUALIZATION/SAMPLOT/HIFIASM/HAP2"
params.mf = "${params.sampledir}/VISUALIZATION/MOMIG/FLY"
params.mhh1 = "${params.sampledir}/VISUALIZATION/MOMIG/HIFIASM/HAP1" 
params.mhh2 = "${params.sampledir}/VISUALIZATION/MOMIG/HIFIASM/HAP2"

include { WF_FLY_ASSEMBLY } from './sv_calling_assembly'
include { WF_HIFIASM_ASSEMBLY } from './sv_calling_assembly'
include { WF_PHASEBOOK_ASSEMBLY } from './sv_calling_assembly'


workflow FLY_ASSEMBLY {   
    WF_DESCLEN(fastq_ch)
    if(params.filt){
        WF_FILTER_READS(fastq_ch)
        WF_FLY_ASSEMBLY(WF_FILTER_READS.out)
    } else {
        WF_FLY_ASSEMBLY(fastq_ch)
    }         
} 

workflow HIFIASM_ASSEMBLY {   
    WF_DESCLEN(fastq_ch)
    if(params.filt){
        WF_FILTER_READS(fastq_ch)
        WF_HIFIASM_ASSEMBLY(WF_FILTER_READS.out)
    } else {
        WF_HIFIASM_ASSEMBLY(fastq_ch)
    }         
} 

workflow PHASEBOOK_ASSEMBLY {   
    WF_DESCLEN(fastq_ch)
    if(params.filt){
        WF_FILTER_READS(fastq_ch)
        WF_PHASEBOOK_ASSEMBLY(WF_FILTER_READS.out)
    } else {
        WF_PHASEBOOK_ASSEMBLY(fastq_ch)
    }         
} 

