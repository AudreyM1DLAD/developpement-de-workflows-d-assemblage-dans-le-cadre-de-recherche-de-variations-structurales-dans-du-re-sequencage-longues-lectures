#!~/achathua/bin/nextflow
nextflow.enable.dsl=2

process convert_gfa_to_fasta {
    
    publishDir "${params.sampledir}/ASSEMBLY/HIFIASM", mode: 'copy', overwrite: true

    input:
    	path hifiasm_haplotypes
    
    output:
	path '*.fasta'
    	
    script:
	"""
	
	awk '/^S/ {print ">"\$2; print \$3}' ${hifiasm_haplotypes} > ${hifiasm_haplotypes}.fasta
	
	for file in *.gfa.fasta; do
    		mv "\$file" "\${file%.gfa.fasta}.fasta"
	done
	      
	"""
}