// Here is a draft file. It contains a part of the commmands I launched in the Terminal to do some tests. If it works the mention "OK" is added at the end of the comment line. 


// QUAST Test on both haplotypes from Hifiasm = OK

jobify -b -c 36 --mem-per-cpu=2G "quast.py true_HG002_0_5X_hap1.fa -o /env/cng/proj/LBA/scratch/achathua/hifiasm/HG002_Quast/HG002_withref/HG002_hap1 -r /env/cng/proj/LBA/scratch/achathua/GRCh38_latest_genomic.fna.gz -t 36 --large --eukaryote --circos"

jobify -b -c 36 --mem-per-cpu=2G "quast.py true_HG002_0_5X_hap2.fa -o /env/cng/proj/LBA/scratch/achathua/hifiasm/HG002_Quast/HG002_withref/HG002_hap2 -r /env/cng/proj/LBA/scratch/achathua/GRCh38_latest_genomic.fna.gz -t 36 --large --eukaryote --circos"

// The 2 jobs last more or less : 20 minutes. 


// Samplot Test from the tool Github page. 
time samplot plot \
    -n NA12878 NA12889 NA12890 \
    -b samplot/test/data/NA12878_restricted.bam \
      samplot/test/data/NA12889_restricted.bam \
      samplot/test/data/NA12890_restricted.bam \
    -o 4_115928726_115931880.png \
    -c chr4 \
    -s 115928726 \
    -e 115931880 \
    -t DEL



// Circos Test = OK 

jobify -b -c 12 "circos –conf circos.conf " 
// The job last more or less : 2 minutes.