#!/usr/bin/env python3

# Colab : 3.10.12

# CIRCOS RAGOO & SNIFFLES2


import sys
import pandas as pd
from pycirclize import Circos
from pycirclize.utils import ColorCycler, load_eukaryote_example_dataset

from matplotlib.patches import Patch
from matplotlib.lines import Line2D

# Charger les fichiers BED des chromosomes et cytoband
chr_bed_file, cytoband_file, _ = load_eukaryote_example_dataset("hg38")

# Charger les données des variants structurels
variants_file = sys.argv[1]
if variants_file.endswith(".bed"): # Pour Ragoo
	variants_data = pd.read_csv(variants_file, sep='\t', header=None, skiprows=1)
elif variants_file.endswith(".vcf"): # Pour Sniffles2
	variants_data = pd.read_csv(variants_file, sep='\t', header=None, comment="#")

# Initialiser Circos à partir du fichier BED des chromosomes
circos = Circos.initialize_from_bed(chr_bed_file, space=3)  # Objet Circos initialisé
circos.text("Reference : Homo sapiens (hg38)\n \nTarget : Sample", deg=315, r=150, size=10)  # Titre

# Accéder aux secteurs auto-définis par le BED
sectors = circos.sectors

# Ajouter les tracks des cytobands
outer_track = circos.add_cytoband_tracks((95, 100), cytoband_file)

# Créer un dictionnaire de couleurs pour les chromosomes
ColorCycler.set_cmap("hsv")
chr_names = [s.name for s in circos.sectors]
colors = ColorCycler.get_color_list(len(chr_names))
chr_name2color = {name: color for name, color in zip(chr_names, colors)}

# Plotter les noms des chromosomes et les marques d'échelle
for sector in circos.sectors:
    sector.text(sector.name, r=120, size=10, color=chr_name2color[sector.name])
    sector.get_track("cytoband").xticks_by_interval(
        40000000,
        label_size=8,
        label_orientation="vertical",
        label_formatter=lambda v: f"{v / 1000000:.0f} Mb",
    )

# Il faut considérer 1 secteur comme un plan individuel. 



for sector in sectors:
# Ajouter les tracks pour les différents types de variants structurels
  ins_track = sector.add_track((85, 90), name="Insertion")  # Insertions
  ins_track.axis(fc="gainsboro")

  del_track = sector.add_track((75, 80), name="Deletion")  # Délétions
  del_track.axis()

  tandexp_track = sector.add_track((65, 70), name="Tandem_expansion")  # Expansions en tandem
  tandexp_track.axis(fc="gainsboro")

  repexp_track = sector.add_track((55, 60), name="Repeat_expansion")  # Expansions répétées
  repexp_track.axis()

  repcon_track = sector.add_track((45, 50), name="Repeat_contraction")  # Contractions répétées  
  repcon_track.axis(fc="gainsboro")


# Ajout des SVs

# Il y a le nom des secteurs et le nom des tracks. 
# Les rectangles ça marche pas. Les points c'est bon. Le pb c'était ça plot tout sur chrY
# => C'est pcq il savait pas dans quel secteur mettre. 
# Faut préciser secteur ET track

for index, variant in variants_data.iterrows():
    if variants_file.endswith(".bed"):
      chr, start, stop, sv_type = variant[0], int(variant[1]), int(variant[2]), variant[6]
      for sector in sectors:
          if sector.name == chr:
            try:
              mid_point = (start + stop)/2
              if sv_type == "Insertion":
                sector.get_track("Insertion").scatter([mid_point], [0], c="red")
              elif sv_type == "Deletion":
                sector.get_track("Deletion").scatter([mid_point], [0], c="blue")
              elif sv_type == "Repeat_contraction":
                sector.get_track("Repeat_contraction").scatter([mid_point], [0], c="lightseagreen")
              elif sv_type == "Repeat_expansion":
                sector.get_track("Repeat_expansion").scatter([mid_point], [0], c="hotpink")
              elif sv_type == "Tandem_expansion":
                sector.get_track("Tandem_expansion").scatter([mid_point], [0], c="indigo")
              else:
                print("Encountered an anomaly")
            except ValueError as e:
              print(f"Can't plot variant at line {index}: {e}")
              continue # Continue malgré l'erreur
    elif variants_file.endswith(".vcf"):
      chr, pos, sv_type = variant[0], int(variant[1]), variant[2]
      for sector in sectors:
          if sector.name == chr:
            try:
              if sv_type.startswith("Sniffles2.INS"):
                sector.get_track("Insertion").scatter([pos], [0], c="red")
              elif sv_type.startswith("Sniffles2.DEL"):
                sector.get_track("Deletion").scatter([pos], [0], c="blue")
              else:
                print("Encountered an anomaly")
            except ValueError as e:
              print(f"Can't plot variant at line {index}: {e}")
              continue # Continue malgré l'erreur
      
      

# Afficher le plot
fig = circos.plotfig()

_ = circos.ax.legend(
    handles=[
        Patch(color="red", label="Insertion"),
        Patch(color="blue", label="Deletion"),
        Patch(color="indigo", label="Tandem_expansion"),
        Patch(color="hotpink", label="Repeat_expansion"),
        Patch(color="lightseagreen", label="Repeat_contraction"),
    ],
    bbox_to_anchor=(0.5, 0.5),
    loc="center",
    ncols=1,
    title="Structural Variations",
    fontsize=8,
)

# Save the plot
fig.savefig("circos.png", dpi=100)




# *** To save a figure with legend as a file, use `fig.savefig()` method ***
# *** `circos.savefig()` method does not save the figure with legend ***
# fig.savefig("result.png", dpi=100)

       
       
