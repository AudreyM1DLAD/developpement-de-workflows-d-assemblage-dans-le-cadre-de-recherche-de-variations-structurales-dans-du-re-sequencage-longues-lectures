#  Development of assembly workflows for structural variations identification in long reads re-sequencing 



## Abstract

The identification of structural variations (SVs) allows us to better understand evolution and certain genetic diseases. Several approaches have made it possible to study these variations by attempting to overcome numerous limits imposed by nature such as noise due to repeated sequences which represent more than 50% of the human genome.


GEXPLOR is a tool developed in the CNRGH allowing the exploration of SVs through a mapping and/or assembly approach which presents numerous prospects for evolution and improvement. First, the assembly is carried out by Flye, a de novo assembler for ONT reads. Despite its performance, Flye is not suitable for polyploid organisms. Indeed, it collapses the two haplotypes of diploid genomes into a consensus sequence. To address these issues, Hifiasm and Phasebook, two de novo assemblers that support diploid genomes are added to GEXPLOR. On the other hand, new datasets representative of the human genome are generated in order to test the entire pipeline in a reasonable time. Finally, a tool allowing a certain type of visualization of SVs is tested and added (pyCirclize).




## Description

In this Git repository, you'll see almost all the contributions I added on GEXPLOR. 

Here are a presentation of each file (except the ReadMe) : 
1. [convert.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/convert.nf?ref_type=heads) = the Nextflow script containing the process of file conversion (gfa to fa)

2. [generate_datatest.sh](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/generate_datatest.sh?ref_type=heads) = the Bash script containing all the commands used to generate new datatest using several tools

3. [gexplor.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/gexplor.nf?ref_type=heads) = the part I add to the main Nextflow script of GEXPLOR which is all the parameters (file path here) and the definition of assembler-specific workflows. 

4. [othercallers.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/othercallers.nf?ref_type=heads) = the Nextflow script containing processes for an other SV-Caller than Ragoo. Because Ragoo is obsolete and we trying to find a good alternative. 

5. [pycirclize_ragoo.py](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/pycirclize_ragoo.py?ref_type=heads) = the Python script added to GEXPLOR in order to generate Circos Plot of SVs for visualization. 

6. [ragoo.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/ragoo.nf?ref_type=heads) = the part I modify about Ragoo. 

7. [sv_calling_assembly.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/sv_calling_assembly.nf?ref_type=heads) = the part I add to this Nextflow script of GEXPLOR which is a huge processes call and the definition of workflows and processes assembler-specific. 

8. [test.sh](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/test.sh?ref_type=heads) = a Bash script containing some commands I launched to test a few tools. 

9. [visualization.nf](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/visualization.nf?ref_type=heads) = the Nextflow script I create, containing processes for different visualization tools. 

10.  [SVs_classification.py](https://gitlab.com/AudreyM1DLAD/developpement-de-workflows-d-assemblage-dans-le-cadre-de-recherche-de-variations-structurales-dans-du-re-sequencage-longues-lectures/-/blob/main/SVs_classification.py?ref_type=heads) = the Python script added to GEXPLOR in order to generate a small SVs table classifying the SVs by size class.  _(added after report submission)_


## Example of launch to test the GEXPLOR pipeline 

Here is an example of commands used to launch the GEXPLOR pipeline : 

```
export GEXPLOR="/env/cng/proj/LBA/scratch/achathua/GEXPLOR/gexplor"; $GEXPLOR/scripts/config_test.sh -h inti -d $BIODIR -c $GEXPLOR/datatest/DEVPROJECT_SV_analysis.patient_chr20_16X_ONT.projectconfig -f 500 -t ONT; module load java/11; export SAMPLE=patient_chr20_16X_ONT; export SVDIR=$BIODIR/SV_ANALYSIS/; export NXF_WORK=$SVDIR/$SAMPLE;
jobify -b -c 24 --time=1440 --mem-per-cpu=2G "~achathua/bin/nextflow run $GEXPLOR/nextflow/gexplor.nf -config $SVDIR/CONFIG/${SAMPLE}_sv_analysis.config -entry HIFIASM_ASSEMBLY 1>$SAMPLE.assembly.nextflow.out 2>$SAMPLE.assembly.nextflow.err"
```
The first line allows to export the variables required, load the java module and generate a configuration file data-specific based on the projectconfig file written in advance. 

The second line is the launch of the pipeline (here, only the Hifiasm workflow) using the previous configuration file and redirect the standard and error output to output files. 
The command is wrapped by Jobify to simplify the lauch and the allocation of ressources. 

## Support
For any help please contact the author of this project

## Authors and acknowledgment
CHATHUANT Audrey 

Internship M2 : Software Development and Data Analysis (AMU) 

## Project status
The development is still in progress since the internship is not finished yet.  
